/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalTime;

/**
 *
 * @author USUARIO
 */
public class Usuario implements Comparable<Usuario> {

    private int cedula;
    private String nombre;
    private String email;
    private LocalTime fechaDeRegistro;
    private byte cantInfantes, cantAdolescentes;

    public Usuario() {
    }

    public Usuario(int cedula, String nombre, String email, LocalTime fechaDeRegistro, byte cantInfantes, byte cantAdolescentes) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.email = email;
        this.fechaDeRegistro = fechaDeRegistro;
        this.cantInfantes = cantInfantes;
        this.cantAdolescentes = cantAdolescentes;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalTime getFechaDeRegistro() {
        return fechaDeRegistro;
    }

    public void setFechaDeRegistro(LocalTime fechaDeRegistro) {
        this.fechaDeRegistro = fechaDeRegistro;
    }

    public byte getCantInfantes() {
        return cantInfantes;
    }

    public void setCantInfantes(byte cantInfantes) {
        this.cantInfantes = cantInfantes;
    }

    public byte getCantAdolescentes() {
        return cantAdolescentes;
    }

    public void setCantAdolescentes(byte cantAdolescentes) {
        this.cantAdolescentes = cantAdolescentes;
    }

    @Override
    public String toString() {
        return "\nUsuario: " + "C.C:" + cedula + ", Nombre: " + nombre + ", E-Mail: " + email + ", Fecha de Registro: " + fechaDeRegistro + ", Cantidad de Infantes: " + cantInfantes + ", Cantidad de Adolescentes: " + cantAdolescentes + "\n";
    }

    @Override
    public int compareTo(Usuario o) {
        return this.fechaDeRegistro.compareTo(o.fechaDeRegistro);
    }

    //12:50:30 -> 125030
    public int prioridad(String hora) {
        String prioridad = hora.toString().replaceAll(":", "");
        if (prioridad.length() == 5) {
            prioridad = prioridad + "00";
        }

        return Integer.valueOf(prioridad);
    }

}

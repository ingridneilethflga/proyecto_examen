/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Notificacion {

    private String descripcion;

    public Notificacion(Usuario usu, byte compureci, byte tabletsreci, boolean tipo) {
        byte tablet2 = usu.getCantInfantes();
        byte H = usu.getCantAdolescentes();
        byte compu2 = 0;
        if (H % 2 == 0) {
            compu2 = (byte) (H / 2);
        } else if (H != 1) {
            compu2 = (byte) ((H / 2) + 1);
        }

        if (tipo) {
            this.descripcion = "El usuario " + usu.getNombre() + " con cedula " + usu.getCedula();
        }

        if (compu2 == compureci) {
            if (tipo) {
                this.descripcion += " se asigno " + compureci + " computadores y/o " + tabletsreci + " tablets";
            } else {
                this.modificar(usu, compu2, tablet2, compureci, tabletsreci, (byte) 1);
            }

        } else if ((compureci != 0 || tabletsreci != 0) && (tablet2 != 0 || compu2 != 0)) {
            if (tipo) {
                this.descripcion += " se asigno " + compureci + " computadores y/o " + tabletsreci + " tablets"
                        + " PERO le hicieron falta " + (compu2 - compureci) + " y/o " + (tablet2 - tabletsreci) + " tablets";
            } else {
                this.modificar(usu, compu2, tablet2, compureci, tabletsreci, (byte) 2);
            }
        } else {
            if (tipo) {
                this.descripcion += " NO SE LE ASIGNARON NI COMPUTADORES NI TABLETS, INVENTARIO VACÍO.";
            } else {
                this.modificar(usu, compu2, tablet2, compureci, tabletsreci, (byte) 3);
            }
        }
    }

    private void modificar(Usuario u, byte compu2, byte tablet2, byte compusreci, byte tabletsrecie, byte caso) {
        this.descripcion = "" + u.getCedula();
        if (caso == 1 || caso == 2) {
            this.descripcion += ";" + compu2 + ";" + tablet2 + ";" + (compu2 - compusreci) + ";" + (tablet2 - tabletsrecie);
        } else {
            this.descripcion += ";" + "-" + ";" + "-" + ";" + "-" + ";" + "-";
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "\n" + this.descripcion;
    }

}
